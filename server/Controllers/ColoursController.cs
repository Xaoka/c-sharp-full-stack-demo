﻿using System;
using System.Collections.Generic;
using System.Linq;
// using System.Net.Http;
// using System.Web.Http;
// using System.Web.Http.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace server.Controllers
{
    [ApiController]
    [Route("[controller]")]
    // [EnableCors(origins: "http://localhost:3000", headers: "*", methods: "*")]

    public class ColoursController : ControllerBase
    {
        private static readonly string[] Colours = new[]
        {
            "Cyan", "Purple", "Teal"
        };

        private readonly ILogger<String> _logger;

        public ColoursController(ILogger<String> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<String> Get()
        {
            return Colours.ToArray();
        }
    }
}
