﻿using System;
using System.IO;
// using System.Net.Http;
// using System.Web.Http;
// using System.Web.Http.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace server.Controllers
{
    [ApiController]
    [Route("[controller]")]
    // [EnableCors(origins: "http://localhost:3000", headers: "*", methods: "*")]

    public class FormController : ControllerBase
    {
        private readonly ILogger<String> _logger;

        public FormController(ILogger<String> logger)
        {
            _logger = logger;
        }

        [HttpPost]
        public void Post(FormData data)
        {
            string docPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            using (StreamWriter outputFile = new StreamWriter(Path.Combine(docPath, "RowanFormData.txt")))
            {
                outputFile.WriteLine(data.firstName);
                outputFile.WriteLine(data.lastName);
                outputFile.WriteLine(data.dob);
                outputFile.WriteLine(data.colour);
                outputFile.Flush();
            }
        }
    }

    public class FormData {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string dob { get; set; }
        public string colour { get; set; }
    }
}
