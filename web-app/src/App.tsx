import './App.css';
import { useEffect, useState } from 'react';
import Form, { IFormData } from './Form';
import { Snackbar } from '@material-ui/core';
import MuiAlert from '@material-ui/lab/Alert';

function App() {

  const [loading, setLoading] = useState(true);
  const [colours, setColours] = useState([]);
  const [snackbarOpen, setSnackbarOpen] = useState(false);


  useEffect(() => {
    const req = new XMLHttpRequest();
    req.open("GET", "http://localhost:5000/Colours");
    req.send();
    req.onreadystatechange = () => {
      // TODO: State
      if (req.responseText.length === 0) { return; }
      const cols = JSON.parse(req.responseText);
      console.dir(cols);
      setColours(cols);
      setLoading(false);
    }
  }, [])

  function onFormSubmit(data: IFormData)
  {
    const req = new XMLHttpRequest();
    req.open("POST", "http://localhost:5000/Form");
    req.setRequestHeader("Content-Type", "application/json");
    req.send(JSON.stringify(data));
    setSnackbarOpen(true);
  }

  return (<>
    <h1>Rowan <span style={{ color: "darkgreen" }}>Powell</span></h1>
    <h2>C# with React full stack demo</h2>
    <Form loading={loading} colours={colours} onSubmit={onFormSubmit}/>
    <Snackbar open={snackbarOpen} autoHideDuration={6000} onClose={() => setSnackbarOpen(false)}>
      <MuiAlert elevation={6} variant="filled" onClose={() => setSnackbarOpen(false)} severity="success">
        Form Data submitted!
      </MuiAlert>
    </Snackbar>
    </>
  );
}

export default App;
