
import { CircularProgress, FormControl, InputLabel, MenuItem, Select, TextField } from '@material-ui/core';
import React, { useState, useEffect } from 'react';

export interface IFormData
{
    firstName: string;
    lastName: string;
    dob: string;
    colour: string;
}

interface IForm
{
    loading: boolean;
    colours: string[];
    onSubmit: (data: IFormData) => void;
}

export default function Form(props: IForm)
{
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [dob, setDOB] = useState("");
    const [colour, setColour] = useState("");

    useEffect(() => {
        if (props.colours.length === 0) { return; }
        setColour(props.colours[0]);
    }, [props.colours])

    function onSubmit(event: React.FormEvent<HTMLFormElement>)
    {
        event.stopPropagation();
        event.preventDefault();

        // Could add some validation here

        console.dir(event);
        props.onSubmit({ firstName, lastName, dob, colour })
    }

    if (props.loading)
    {
        return <CircularProgress/>
    }
    else
    {
        return <div style={{ paddingLeft: 100, paddingRight: 100 }}>
          <form onSubmit={onSubmit}>
            <TextField label="First Name" fullWidth={true} onChange={(evt) => setFirstName(evt.target.value)}/>
            <TextField label="Last Name" fullWidth={true} onChange={(evt) => setLastName(evt.target.value)}/>
            <TextField label="Date Of Birth" fullWidth={true} onChange={(evt) => setDOB(evt.target.value)}/>
            <FormControl fullWidth={true}>
                <InputLabel>Colour of choice</InputLabel>
                <Select value={colour} onChange={(evt) => setColour(evt.target.value as string)}>
                {props.colours.map((col) => <MenuItem key={col} value={col}>{col}</MenuItem>)}
                </Select>
            </FormControl>
            <button>Submit</button>
          </form>
        </div>
    }
}